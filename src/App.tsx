import { Layout } from "antd";
import ConnectBar from "./components/connect";
import HouseList from "./components/house";
import PostCode from "./components/postCode";

const { Content, Footer } = Layout;

function App() {
  return (
    <Layout style={{ padding: "0 50px" }}>
      <ConnectBar />

      <Content>
        <HouseList />
      </Content>
      <Footer style={{ textAlign: "center" }}>
        <PostCode />
      </Footer>
    </Layout>
  );
}

export default App;
