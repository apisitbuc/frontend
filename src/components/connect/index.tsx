import { Button, Col, Input, Row } from "antd";

const ConnectBar = () => {
  return (
    <Row gutter={[32, 32]}>
      <Col span={8}>
        <Row>
          <Col span={24}>URL</Col>
          <Col span={24} style={{ margin: "5px" }}>
            <Input placeholder="http://localhost" size="large" />
          </Col>
        </Row>
      </Col>

      <Col span={8}>
        <Row>
          <Col span={24}>PORT</Col>
          <Col span={24} style={{ margin: "5px" }}>
            <Input placeholder="8000" size="large" />
          </Col>
        </Row>
      </Col>
      <Col span={8}>
        <Row>
          <Col span={24} style={{ margin: "5px", marginTop: "20px" }}>
            <Button type="primary" block size="large">
              CONNECT
            </Button>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default ConnectBar;
