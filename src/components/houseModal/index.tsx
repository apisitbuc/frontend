import { Button, Col, Input, Modal, ModalProps, Row } from "antd";
import { ACTION } from "../house";
import { IHouse } from "../../service/house.service";

const { TextArea } = Input;

export interface IHouseModal extends ModalProps {
  isShowModal: boolean;
  houseDetail?: IHouse;
  action: ACTION;
}

const HouseModal: React.FC<IHouseModal> = (props) => {
  const { isShowModal, onCancel, onOk, houseDetail, action } = props;

  const shouldDisplayTitle = () => {
    return action === ACTION.CREATE
      ? "Create"
      : `Item Detail - ${houseDetail?.id}`;
  };

  const shouldDisplayButton = () => {
    return action === ACTION.CREATE ? ACTION.CREATE : ACTION.UPDATE;
  };

  return (
    <>
      <Modal
        title={shouldDisplayTitle()}
        open={isShowModal}
        onOk={onOk}
        onCancel={onCancel}
        centered
        footer={[
          <Button onClick={() => onCancel}>CANCEL</Button>,
          <Button
            key="submit"
            onClick={() => onOk}
            style={{
              backgroundColor: action === ACTION.CREATE ? "green" : "orange",
              color: "white",
            }}
          >
            {shouldDisplayButton()}
          </Button>,
        ]}
      >
        <Row gutter={[12, 12]}>
          <Col span={12}>Name</Col>
          <Col span={6}>Post Code</Col>
          <Col span={6}>Price</Col>
        </Row>
        <Row gutter={[8, 8]}>
          <Col span={12}>
            <Input
              placeholder="Name"
              size="large"
              defaultValue={houseDetail?.name}
            />
          </Col>
          <Col span={6}>
            <Input
              placeholder="Post Code"
              size="large"
              defaultValue={houseDetail?.post_code}
            />
          </Col>
          <Col span={6}>
            <Input
              placeholder="Price"
              size="large"
              defaultValue={houseDetail?.price}
            />
          </Col>
        </Row>
        <Row>
          <Col span={24}>Description</Col>
        </Row>
        <Row>
          <Col span={24}>
            <TextArea
              maxLength={100}
              style={{ height: 120, marginBottom: 12 }}
              defaultValue="Lorem ipsum dolor"
              placeholder="Description"
            />
          </Col>
        </Row>
      </Modal>
    </>
  );
};

export default HouseModal;
