import { Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import { HouseService } from "../../service/house.service";

const houseService = new HouseService();

const PostCode = () => {
  const [postCodes, setPostCodes] = useState<string[]>([]);

  useEffect(() => {
    async function fetchPostCodes() {
      const _postCodes = await houseService.fetchPostCode();
      setPostCodes(_postCodes);
    }

    fetchPostCodes();
  }, []);

  const onChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  const onSearch = (value: string) => {
    console.log("search:", value);
  };

  return (
    <>
      <Row>
        <Col span={24}>
          <Select
            showSearch
            placeholder="SELECT POST CODE"
            optionFilterProp="children"
            onChange={onChange}
            onSearch={onSearch}
            filterOption={(input, option) =>
              (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
            }
            options={postCodes.map((postCode) => {
              return { value: postCode, label: postCode };
            })}
            size="large"
          />
        </Col>
      </Row>
    </>
  );
};

export default PostCode;
