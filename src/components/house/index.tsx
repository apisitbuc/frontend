import { Button, Col, Row, Table, Typography } from "antd";
import { useEffect, useState } from "react";
import HouseModal from "../houseModal";
import { HouseService, IAction, IHouse } from "../../service/house.service";

const { Title } = Typography;

const houseService = new HouseService();

export enum ACTION {
  CREATE = "CREATE",
  UPDATE = "UPDATE",
}

const HouseList = () => {
  const [isShowModal, setIsShowModal] = useState(false);
  const [houseDetail, setHouseDetail] = useState<IHouse>();
  const [action, setAction] = useState<ACTION>(ACTION.CREATE);
  const [houseList, setHouseList] = useState<IHouse[]>([]);

  useEffect(() => {
    async function fetchHouseList() {
      const houseList = await houseService.fetchHouseList();
      setHouseList(houseList);
    }

    fetchHouseList();
  }, []);

  const onClickButton = (house: IHouse) => {
    setHouseDetail(house);
    setIsShowModal(true);
    setAction(ACTION.UPDATE);
  };

  const handleOnClickCreate = () => {
    setAction(ACTION.CREATE);
    setIsShowModal(true);
  };

  const handelOk = () => {
    setIsShowModal(false);
  };

  const handleOnCancel = () => {
    setIsShowModal(false);
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Post Code",
      dataIndex: "post_code",
      key: "post_code",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Action",
      key: "actions",
      render: (_: any, record: IHouse) => (
        <>
          {record?.actions.map((action: IAction) => (
            <Button
              style={{
                marginRight: "5px",
                backgroundColor: `${action.bg}`,
                color: `${action.color}`,
              }}
              key={action.name}
              onClick={() => onClickButton(record)}
            >
              {action.name}
            </Button>
          ))}
        </>
      ),
    },
  ];

  return (
    <>
      <Row style={{ marginTop: "100px" }} gutter={[32, 32]}>
        <Col span={8}>
          <Title level={2}>HOUSE LIST</Title>
        </Col>
        <Col span={8} offset={8}>
          <Button
            size="large"
            style={{ backgroundColor: "green", color: "white" }}
            block
            onClick={() => handleOnClickCreate()}
          >
            CREATE
          </Button>
        </Col>
      </Row>

      <Row>
        <Col span={24}>
          <Table
            columns={columns}
            dataSource={houseList}
            pagination={{ pageSize: 5 }}
          />
        </Col>
      </Row>

      <HouseModal
        isShowModal={isShowModal}
        houseDetail={houseDetail}
        onOk={handelOk}
        onCancel={handleOnCancel}
        action={action}
      />
    </>
  );
};

export default HouseList;
