import axios from "axios";

export interface IAction {
  name: string;
  color: string;
  bg: string;
}

export interface IHouse {
  id: number;
  name: string;
  post_code: number;
  price: string;
  actions: IAction[];
}

export interface IHouseResponse {
  data: { homes: IHouse[]; count: number };
  status: string;
  statusCodes: number;
}

export class HouseService {
  public async fetchHouseList(): Promise<IHouse[]> {
    const houseList: IHouseResponse = await axios.get(
      "http://localhost:8000/home",
      {}
    );

    const _houseList = houseList?.data.homes || [];

    return _houseList.map((house) => {
      return {
        ...house,
        actions: [
          {
            name: "VIEW DETAIL",
            color: "orange",
            bg: "rgb(255, 255, 203,0.3)",
          },
          { name: "DELETE", color: "purple", bg: "rgb(254, 227, 248,0.3)" },
        ],
      };
    });
  }

  public async fetchPostCode(): Promise<string[]> {
    const postCodes = await axios.get("http://localhost:8000/postCode", {});
    return postCodes?.data?.payload
  }
}
